<%@ include file="/init.jsp" %>


<!--  quando si inserisce un nuovo task, l'oggetto task passato alla jsp sar� null -->

<%
Task task = (Task) request.getAttribute(WebKeys.TASK);
%>

<liferay-ui:header title='<%= task != null ? task.getTitle() : "new-task" %>' backURL="<%= redirect %>" />

<portlet:actionURL name="updateTask" var="updateTaskURL" >
	<portlet:param name="mvcPath" value="/edit_task.jsp" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</portlet:actionURL>


<aui:form action="<%= updateTaskURL %>" method="post" name="fm" >
	<aui:fieldset>

		<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
		<aui:input type="hidden" name="taskId" value="<%= task == null ? StringPool.BLANK : task.getTaskId() %>" />

		<aui:model-context bean="<%= task %>" model="<%= Task.class %>" />

		<aui:input name="title" />

		<aui:input name="description" />

		<aui:input name="expirationDate" />

		<aui:input name="completed" type="checkbox"/>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" value="save" />

		<aui:button type="cancel" onClick="<%= redirect %>" />
	</aui:button-row>


</aui:form>