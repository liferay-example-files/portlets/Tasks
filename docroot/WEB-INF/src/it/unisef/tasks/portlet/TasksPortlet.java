package it.unisef.tasks.portlet;

import it.unisef.tasks.model.Task;
import it.unisef.tasks.service.TaskLocalServiceUtil;
import it.unisef.tasks.util.WebKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.ant.DeleteTask;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class TasksPortlet
 */

public class TasksPortlet extends MVCPortlet {

	public void updateTask(
			ActionRequest request, ActionResponse response)
		throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
										Task.class.getName(), request);

		long taskId = ParamUtil.getLong(request, "taskId");
		String title = ParamUtil.getString(request, "title");
		String description = ParamUtil.getString(request, "description");

		boolean completed = ParamUtil.getBoolean(request, "completed");

		int expirationDateMonth = ParamUtil.getInteger(request, "expirationDateMonth");
		int expirationDateDay = ParamUtil.getInteger(request, "expirationDateDay");
		int expirationDateYear = ParamUtil.getInteger(request, "expirationDateYear");


		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
										WebKeys.THEME_DISPLAY);

		long userId = themeDisplay.getUserId();

		if(taskId > 0 ) {
			// UpdateURL()
		}
		else {
			TaskLocalServiceUtil.addTask(
					userId, title, description, expirationDateMonth, expirationDateDay,
					expirationDateYear, completed, serviceContext);
		}

	}


	public void deleteTask(ActionRequest request, ActionResponse response)
				throws Exception {


			long taskId = ParamUtil.getLong(request, "taskId");

			TaskLocalServiceUtil.deleteTask(taskId);
			
	}


@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

			try {
				Task task = null;

				long taskId = ParamUtil.getLong(request, "taskId");

				if(taskId > 0) {
					task = TaskLocalServiceUtil.getTask(taskId);
				}

				request.setAttribute(WebKeys.TASK, task);

			}
			catch(Exception e ) {
				throw new PortletException();
			}

			// TODO Auto-generated method stub
			super.render(request, response);

			_log.info("TasksPortlet.render()");
	}

	private static Log _log = LogFactoryUtil.getLog(TasksPortlet.class);
}
