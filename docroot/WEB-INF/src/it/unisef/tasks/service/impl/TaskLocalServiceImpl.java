/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.tasks.service.impl;

import java.util.Calendar;
import java.util.Date;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

import it.unisef.tasks.TaskTitleException;
import it.unisef.tasks.model.Task;
import it.unisef.tasks.service.base.TaskLocalServiceBaseImpl;
import it.unisef.tasks.service.persistence.TaskPersistence;

/**
 * The implementation of the task local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.tasks.service.TaskLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.tasks.service.base.TaskLocalServiceBaseImpl
 * @see it.unisef.tasks.service.TaskLocalServiceUtil
 */
public class TaskLocalServiceImpl extends TaskLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.tasks.service.TaskLocalServiceUtil} to access the task local service.
	 */

	@Override
	@Indexable(type = IndexableType.REINDEX)
		
	public Task addTask(
			long userId, String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear,
				boolean completed, ServiceContext serviceContext)
				throws PortalException, SystemException {

				// Non richiamare i metodi di LocalServiceUtil, non serve

			User user = userPersistence.findByPrimaryKey(userId);

			long groupId = serviceContext.getScopeGroupId();

			validate(title);

			Date now = new Date();

			long taskId = counterLocalService.increment();

			// Creazione del Bean

			Task task = taskPersistence.create(taskId);

			task.setGroupId(groupId);
			task.setCompanyId(user.getCompanyId());
			task.setUserId(user.getUserId());
			task.setUserName(user.getFullName());
			
			task.setCreateDate(serviceContext.getCreateDate(now));
			task.setModifiedDate(serviceContext.getModifiedDate(now));

			Calendar expirationDate = CalendarFactoryUtil.getCalendar();

			expirationDate.set(Calendar.MONTH, expirationDateMonth);
			expirationDate.set(Calendar.DAY_OF_MONTH,  expirationDateDay);
			expirationDate.set(Calendar.YEAR, expirationDateYear);
			expirationDate.set(Calendar.HOUR_OF_DAY, 0);
			expirationDate.set(Calendar.MINUTE, 0);
			expirationDate.set(Calendar.SECOND, 0);

			task.setTitle(title);
			task.setDescription(description);

			task.setCompleted(completed);

			task.setExpirationDate(expirationDate.getTime());
			
			task = taskPersistence.update(task, false); // false
														// non fa il merge dei dati che eventualmente gi� ci sono


			return task;
	}

	

	protected void validate(String title) throws PortalException {

		// Guardare Validator (Classe Liferay)

		if(Validator.isNull(title)) {
			throw new TaskTitleException();
		}

	}

}