<%@include file="/init.jsp" %>

<liferay-portlet:renderURL var="editURL">
		<liferay-portlet:param name="mvcPath" value="/edit_task.jsp" />
		<liferay-portlet:param name="redirect" value="<%= currentURL %>" />
</liferay-portlet:renderURL>

<aui:button name="add" onClick="<%= editURL %>" value="add" />

<liferay-ui:search-container emptyResultsMessage="task-empty-results-message">

		<liferay-ui:search-container-results>

			<% 	results =  TaskLocalServiceUtil.getTasks(searchContainer.getStart(),searchContainer.getEnd());

				total = TaskLocalServiceUtil.getTasksCount();

				pageContext.setAttribute("results", results);
				pageContext.setAttribute("total", total);

			%>
		</liferay-ui:search-container-results>

		<!-- modelVar : Bean da cui prendere la proprietÓ specificata in keyProperty  -->

			<liferay-ui:search-container-row
				className="it.unisef.tasks.model.Task"
				keyProperty="taskId"
				modelVar="task"
			>

				<liferay-ui:search-container-column-text
					name="taskId"
					property="taskId"
				/>

				<liferay-ui:search-container-column-text
					name="title"
					property="title"
				/>

				<liferay-ui:search-container-column-text
					name="description"
					property="description"
				/>

				<liferay-ui:search-container-column-text
					name="completed"
					property="completed"
				/>

				<liferay-ui:search-container-column-jsp
					align="right"
					path="/task_action.jsp"
				/>

			</liferay-ui:search-container-row>

	<liferay-ui:search-iterator/>
	
</liferay-ui:search-container>



