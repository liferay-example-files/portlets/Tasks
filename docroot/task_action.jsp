<%@include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Task task = (Task)row.getObject();

	long taskId = task.getTaskId();
%>

<liferay-ui:icon-menu>
		<portlet:renderURL var="updateTaskURL">
			<portlet:param name="mvcPath" value="/edit_task.jsp"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
			<portlet:param name="taskId" value="<%= String.valueOf(taskId) %>"/>
		</portlet:renderURL>

	<liferay-ui:icon image="edit" url="<%= updateTaskURL.toString() %>"/>

		<portlet:actionURL name="deleteTask" var="deleteTaskURL">
			<portlet:param name="taskId" value="<%= String.valueOf(taskId) %>"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
		</portlet:actionURL>

	<liferay-ui:icon image="delete" url="<%=deleteTaskURL.toString() %>"/>

</liferay-ui:icon-menu>